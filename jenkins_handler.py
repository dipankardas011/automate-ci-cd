import jenkins, os

class Jenkins:
    def __init__(self):
        username = os.environ["JENKINS_USERNAME"]
        password = os.environ["JENKINS_PASSWORD"]
        jenkins_url = os.environ["JENKINS_URL"]
        if len(username) == 0 or len(password) == 0 or len(jenkins_url) == 0:
            exit(1)
        self.client = jenkins.Jenkins(jenkins_url, username=username, password=password)

    def createJob(self, job_name: str, innerXML: str):
        self.client.create_job(job_name, innerXML)

    def assembler(self, innerXML: str) -> str:
        
        config_xml = f"""<?xml version='1.1' encoding='UTF-8'?>
        <project>
          <actions/>
          <description></description>
          <keepDependencies>false</keepDependencies>
          <properties/>
          <canRoam>true</canRoam>
          <disabled>false</disabled>
          <blockBuildWhenDownstreamBuilding>false</blockBuildWhenDownstreamBuilding>
          <blockBuildWhenUpstreamBuilding>false</blockBuildWhenUpstreamBuilding>
          <concurrentBuild>false</concurrentBuild>

        {innerXML}
        <buildWrappers/>
        </project>"""

        return config_xml

    def gitRepo(self, url: str, branch: str) -> str:
        return f"""
          <scm class="hudson.plugins.git.GitSCM" plugin="git@4.8.2">
            <configVersion>2</configVersion>
            <userRemoteConfigs>
              <hudson.plugins.git.UserRemoteConfig>
                <url>{url}</url>
              </hudson.plugins.git.UserRemoteConfig>
            </userRemoteConfigs>
            <branches>
              <hudson.plugins.git.BranchSpec>
                <name>*/{branch}</name>
              </hudson.plugins.git.BranchSpec>
            </branches>
            <doGenerateSubmoduleConfigurations>false</doGenerateSubmoduleConfigurations>
            <submoduleCfg class="list"/>
            <extensions/>
          </scm>"""

    def cronTrigger(self, cronRegex: str) -> str:
        return f"""
          <triggers>
        <hudson.triggers.SCMTrigger>
          <spec>{cronRegex}</spec>
          <ignorePostCommitHooks>false</ignorePostCommitHooks>
        </hudson.triggers.SCMTrigger>
      </triggers>"""
    
    def buildWrapper(self, buildTemplate: str) -> str:
        return f"""
            <builders>
            {buildTemplate}
            </builders>"""

    def buildScript(self, script: str) -> str:
        return f"""
        <hudson.tasks.Shell>
          <command>{script}</command>
        </hudson.tasks.Shell>"""

    def postBuildWrapper(self, publisherTemplate: str) -> str:
        return f"""
            <publishers>
            {publisherTemplate}
            </publishers>"""

    def emailPostBuild(self, email_recipients: str, email_subject: str, email_body: str, attachment_pattern: str) -> str:
        return f"""
        <hudson.plugins.emailext.ExtendedEmailPublisher plugin="email-ext@2.83">
          <recipientList>{email_recipients}</recipientList>
          <configuredTriggers>
            <hudson.plugins.emailext.plugins.trigger.AlwaysTrigger>
              <email>
                <subject>{email_subject}</subject>
                <body>{email_body}</body>
                <recipientProviders>
                  <hudson.plugins.emailext.plugins.recipients.ListRecipientProvider/>
                </recipientProviders>
              </email>
            </hudson.plugins.emailext.plugins.trigger.AlwaysTrigger>
          </configuredTriggers>
          <contentType>text/html</contentType>
          <defaultSubject>{email_subject}</defaultSubject>
          <defaultContent>{email_body}</defaultContent>
          <attachmentsPattern>{attachment_pattern}</attachmentsPattern>
          <presendScript></presendScript>
          <postsendScript></postsendScript>
          <sendToDevelopers>false</sendToDevelopers>
          <sendToRequester>false</sendToRequester>
          <sendToCulprits>false</sendToCulprits>
          <sendToRecipientList>true</sendToRecipientList>
        </hudson.plugins.emailext.ExtendedEmailPublisher>"""


