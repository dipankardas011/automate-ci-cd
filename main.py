from logging import error
from jenkins_handler import Jenkins
from synk import Synk

class CLI:
    def prompt(self, message: str) -> str:
        return str(input(message))

    def success(self, message: str, linewidth= 20):
        spaceWidth = linewidth - 2 - len(message)
        space = spaceWidth // 2
        message = "|" + "=" * linewidth + "|\n"
        message += "|" + " " * space + message + " " * space + "|\n"
        message += "|" + "=" * linewidth + "|\n"

        print(message)

    def info(self, message: str):
        print(message)

    def error(self, message: str):
        error(message)

    def multilineInput(self, message: str) -> str:
        self.info(message)

        multiline: str = ""
        self.info("Enter EOF to terminate")
        while True:
            line = input()

            if line == "EOF":
                break

            multiline += line + "\n"

        return multiline


if __name__ == '__main__':
    jenkinsClient = Jenkins()

    cli = CLI()

    # NOTE: {build_wrapper} is used to wrap around build targets
    # NOTE: {upper_config} is used to wrap the upper section
    upper_config = ""
    build = ""
    post_build = ""

    job_name = cli.prompt("Project Name.. ? ")


    ############# Upper section of config ##############

    need_git = cli.prompt("Configure Git [y/N] ? ")
    
    if len(need_git) == 0 or need_git == "N" or need_git == "n":
        upper_config += ""
    else:
        url = cli.prompt("\tGit Repo URL ? ")
        branch = cli.prompt("\tGit Repo Branch ? ")
        upper_config += jenkinsClient.gitRepo(url, branch)
    
    need_cron_trigger = cli.prompt("Configure CronJob trigger [y/N] ? ")
    
    if len(need_cron_trigger) == 0 or need_cron_trigger == "N" or need_cron_trigger == "n":
        upper_config += ""
    else:
        cron_time = cli.prompt("\tCron expression ? ")
        upper_config += jenkinsClient.cronTrigger(cron_time)


    ############# Build section of config ##############

    need_build = cli.prompt("Configure Build [y/N] ? ")


    if len(need_build) == 0 or need_build == "N" or need_build == "n":
        build = ""
    else:
        build_no = 1
        while True:
            cli.info(f"Build Step {build_no}")

            option = cli.prompt("Build step\n\t1. Script\n\t2. Snyk\n\n[1/2].. ? ")
            if option == "1":
                script = cli.multilineInput("Bash Script ? ")
                build += jenkinsClient.buildScript(script)
            elif option == "2":
                snyk = Synk()
                
                get_synk_installation: list[str] = snyk.getInstallation(jenkinsClient)
                cli.info(f"Here are the synk installation names\n{get_synk_installation}")
                synk_install_name = cli.prompt("Synk installation name ? ")

                get_synk_token_id: list[str] = snyk.getCredentialID()
                cli.info(f"Here are the synk token id\n{get_synk_token_id}")
                synk_token_id = cli.prompt("Synk Token id? ")

                build += snyk.buildStep(synk_install_name, synk_token_id)
            else:
                cli.error(f"Invalid option {option}")
                # WARN: Here the build no is decremented so that it remains same as wrong option is entered
                build_no -= 1

            build_no+=1 
            continue_step = cli.prompt("Add more Steps [y/N] ? ")
            if len(continue_step) == 0 or continue_step == "n" or continue_step == "N":
                break

    build_wrapper = jenkinsClient.buildWrapper(build)


    need_post_build = cli.prompt("Configure Post-Build [y/N] ? ")
    if len(need_post_build) == 0 or need_post_build == "N" or need_post_build == "n":
        post_build = ""

    else:
        build_no = 1
        while True:
            cli.info(f"Post-Build Step {build_no}")

            option = cli.prompt("Post-Build step\n\t1. Editable Email Notification\n\t2. TBD\n\n[1/2].. ? ")
            if option == "1":
                email_recipients = cli.prompt("Email_recipients ? ")
                email_subject = cli.prompt("Email_subject\nEx. Jenkins Job: ${PROJECT_NAME} - Build #${BUILD_NUMBER} - ${BUILD_STATUS}\n ? ")
                email_body = cli.prompt("Email_body\nEx, Check console output at ${BUILD_URL} to view the results. \n ? ")
                attachment_pattern = cli.prompt("Attachment pattern Ex. *.html ? ")

                post_build += jenkinsClient.emailPostBuild(email_recipients, email_subject, email_body, attachment_pattern)
            elif option == "2":
                post_build += ""
            else:
                cli.error(f"Invalid option {option}")
                # WARN: Here the build no is decremented so that it remains same as wrong option is entered
                build_no -= 1

            build_no+=1 
            continue_step = cli.prompt("Add more Steps [y/N] ? ")
            if len(continue_step) == 0 or continue_step == "n" or continue_step == "N":
                break


    post_build_wrapper = jenkinsClient.postBuildWrapper(post_build)

    xml = jenkinsClient.assembler(upper_config + build_wrapper  + post_build_wrapper)

    jenkinsClient.createJob(job_name, xml)




