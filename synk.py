
import jenkins_handler

class Synk:
    # def __init__(self):
    #     self.message = ""
    def buildStep(self, synk_install_name: str, synk_token_id: str) -> str:
        return f"""
            <io.snyk.jenkins.SnykStepBuilder plugin="snyk-security-scanner@0.11.0">
          <snykInstallation>{synk_install_name}</snykInstallation>
          <snykTokenId>{synk_token_id}</snykTokenId>
          <failOnIssues>false</failOnIssues>
          <failOnError>true</failOnError>
          <organisation></organisation>
          <projectName></projectName>
          <targetFile></targetFile>
          <severity></severity>
          <additionalArguments></additionalArguments>
        </io.snyk.jenkins.SnykStepBuilder>"""

    def getInstallation(self, client: jenkins_handler.Jenkins) -> list[str]:
        installed_plugins = client.client.get_plugins()
        info  = installed_plugins['snyk-security-scanner']
        if info == None:
            return []
        return ["synk for pdf editor", "note these are for demo"]

    def getCredentialID(self) -> list[str]:
        return ["synkapitoken", "note these are for demo"]
